#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library
#include <SD.h>
#include <SPI.h>
#include "menu.h"
// #include <Fonts\FreeSansBoldOblique24pt7b.h>
// The control pins for the LCD can be assigned to any digital or
// analog pins...but we'll use the analog pins as this allows us to
// double up the pins with the touch screen (see the TFT paint example).
#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0
#define PIN_SD_CS 10 // Adafruit SD shields and modules: pin 10

#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

// When using the BREAKOUT BOARD only, use these 8 data lines to the LCD:
// For the Arduino Uno, Duemilanove, Diecimila, etc.:
//   D0 connects to digital pin 8  (Notice these are
//   D1 connects to digital pin 9   NOT in order!)
//   D2 connects to digital pin 2
//   D3 connects to digital pin 3
//   D4 connects to digital pin 4
//   D5 connects to digital pin 5
//   D6 connects to digital pin 6
//   D7 connects to digital pin 7
// For the Arduino Mega, use digital pins 22 through 29
// (on the 2-row header at the end of the board).

// Assign human-readable names to some common 16-bit color values:
#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define BORDER  0x3186
#define INSIDE  0x7BEF
#define TermoValText 0xF81F
#define TermoInfoText 0x07E0
#define infoText 0xFFFF
#define clockText 0xFFE0
;
Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);
// If using the shield, all control and data lines are fixed, and
// a simpler declaration can optionally be used:
// Adafruit_TFTLCD tft;


#define TermoHeight 90
#define TermoWidth 150
#define TermoButon 45
#define BORDERWIDTH 2
#define TEXTMARGIN 7
#define tempValTextSize 7
#define tempInfoTextSize 2
#define ClockTextSize 3
#define timePosX 320-110
#define timePosY 10
#define infoPosX 0
#define infoPosY 10
#define infoTextSize 2
#define MAX_BMP         10                      // bmp file num

#define FILENAME_LEN    20                      // max file name length

const int __Gnbmp_height = 320;                 // bmp hight
const int __Gnbmp_width  = 240;                 // bmp width

unsigned char __Gnbmp_image_offset  = 0;        // offset

int __Gnfile_num = 4;                           // num of file

char __Gsbmp_files[4][FILENAME_LEN] =           // add file name here
{
"flower.bmp",
"tiger.bmp",
"tree.bmp",
"RedRose.bmp",
};
File bmpFile;

/*********************************************/
// This procedure reads a bitmap and draws it to the screen
// its sped up by reading many pixels worth of data at a time
// instead of just one pixel at a time. increading the buffer takes
// more RAM but makes the drawing a little faster. 20 pixels' worth
// is probably a good place

#define BUFFPIXEL       60                      // must be a divisor of 240 
#define BUFFPIXEL_X3    180                     // BUFFPIXELx3

void bmpdraw(File f, int x, int y)
{
    bmpFile.seek(__Gnbmp_image_offset);

    uint32_t time = millis();

    uint8_t sdbuffer[BUFFPIXEL_X3];                 // 3 * pixels to buffer

    for (int i=0; i< __Gnbmp_height; i++) {
        for(int j=0; j<(240/BUFFPIXEL); j++) {
            bmpFile.read(sdbuffer, BUFFPIXEL_X3);
            
            uint8_t buffidx = 0;
            int offset_x = j*BUFFPIXEL;
            unsigned int __color[BUFFPIXEL];
            
            for(int k=0; k<BUFFPIXEL; k++) {
                __color[k] = sdbuffer[buffidx+2]>>3;                        // read
                __color[k] = __color[k]<<6 | (sdbuffer[buffidx+1]>>2);      // green
                __color[k] = __color[k]<<5 | (sdbuffer[buffidx+0]>>3);      // blue
                
                buffidx += 3;
            }

	    for (int m = 0; m < BUFFPIXEL; m ++) {
              tft.drawPixel(m+offset_x, i,__color[m]);
	    }
        }
    }
    
    Serial.print(millis() - time, DEC);
    Serial.println(" ms");
}

boolean bmpReadHeader(File f) 
{
    // read header
    uint32_t tmp;
    uint8_t bmpDepth;
    
    if (read16(f) != 0x4D42) {
        // magic bytes missing
        return false;
    }

    // read file size
    tmp = read32(f);
    Serial.print("size 0x");
    Serial.println(tmp, HEX);

    // read and ignore creator bytes
    read32(f);

    __Gnbmp_image_offset = read32(f);
    Serial.print("offset ");
    Serial.println(__Gnbmp_image_offset, DEC);

    // read DIB header
    tmp = read32(f);
    Serial.print("header size ");
    Serial.println(tmp, DEC);
    
    int bmp_width = read32(f);
    int bmp_height = read32(f);
    
    if(bmp_width != __Gnbmp_width || bmp_height != __Gnbmp_height)  {    // if image is not 320x240, return false
        return false;
    }

    if (read16(f) != 1)
    return false;

    bmpDepth = read16(f);
    Serial.print("bitdepth ");
    Serial.println(bmpDepth, DEC);

    if (read32(f) != 0) {
        // compression not supported!
        return false;
    }

    Serial.print("compression ");
    Serial.println(tmp, DEC);

    return true;
}

/*********************************************/
// These read data from the SD card file and convert them to big endian
// (the data is stored in little endian format!)

// LITTLE ENDIAN!
uint16_t read16(File f)
{
    uint16_t d;
    uint8_t b;
    b = f.read();
    d = f.read();
    d <<= 8;
    d |= b;
    return d;
}

// LITTLE ENDIAN!
uint32_t read32(File f)
{
    uint32_t d;
    uint16_t b;

    b = read16(f);
    d = read16(f);
    d <<= 16;
    d |= b;
    return d;
}
#include"menu.h"
// #include"icons.h"

const char* name = "Badgy";
int offsetX=0;
  int offsetY=15;
  int sizeOfMenuPos=64;
  const int displayWidth=320;
  int positionX=0;
  int positionY=0;


void showInfo(char *text)
{
  
}
void showIP(char urlCharArray[]){
  
}
// 
// SKROLOWANIE DO ZROBIENNIA
// 


/* 
org scroll z neta
void scrolltext(int x, int y, const char *s, uint8_t dw = 1, const GFXfont *f = NULL, int sz = 1)
{
    int16_t x1, y1, wid = tft.width(), inview = 1;
    uint16_t w, h;
    tft.setFont(f);
    tft.setTextColor(YELLOW, BLACK);
    tft.setTextSize(sz);
    tft.setTextWrap(false);
    tft.getTextBounds((char*)s, x, y, &x1, &y1, &w, &h);
    //    w = strlen(s) * 6 * sz;

    for (int steps = wid + w; steps >= 0; steps -= dw) {
        x = steps - w;
        if (f != NULL) {
            inview = wid - x;
            if (inview > wid) inview = wid;
            if (inview > w) inview = w;
            tft.fillRect(x > 0 ? x : 0, y1, inview + dw, h, BLACK);
        }
        x -= dw;
        tft.setCursor(x, y);
        tft.print(s);
        if (f == NULL) tft.print("  "); //rubout trailing chars
        delay(5);
    }
} */

void scrolltext(int x, int y,int boxW, String s, String time, uint8_t dw = 1, const GFXfont *f = NULL, int sz = 1)
{
    int16_t x1, y1, wid =210, inview = 1;
    uint16_t  w,h,wforfor;//txtLen, wTxt;
    String textToShow;
    tft.setFont(f);
    tft.setTextColor(infoText, INSIDE);
    tft.setTextSize(sz);
    tft.setTextWrap(false);

    // 
    tft.getTextBounds(s.c_str(   ), x, y, &x1, &y1, &w, &h); 
    // w = strlen(s.c_str()) * 6 * sz;
   wforfor=w;
    for (int steps = 0; steps <= wid + wforfor; steps += dw) {
    // for (int steps = wid + w; steps >= 0; steps -= dw) {
      // wTxt = strlen(s.c_str()) * 6 * sz;
      // tft.getTextBounds(s.c_str(), x, y, &x1, &y1, &w, &h);
      //             Serial.println("dlugosc str");
      //   Serial.println(wTxt);
      //        Serial.println("okienko");
      //   Serial.println(boxW-steps - w);
      //  if (wTxt > (boxW-steps - w))
      //     {
      //     txtLen=(boxW-steps - w-wTxt)/6*sz;
      //     Serial.print("ile pix  ");
      //     Serial.println(boxW-steps - w-wTxt);
      //      Serial.print("ile znakow ");
      //     Serial.println(txtLen);
      //     textToShow=s.substring(0,txtLen).c_str();
      //     }
      //   else
      //     {
        // if (steps<boxW-x)
        
          textToShow=s.substring(0,steps/10);
        //   Serial.println(steps);
        //    Serial.println(textToShow);
          // }
          
        x =wid -steps ;
        if (f != NULL) {
            inview = wid - x;
            if (inview > wid) inview = wid;
            if (inview > w) inview = w;
            tft.fillRect(x > 0 ? x : 0, y1, inview + dw, h, INSIDE);
            
        }
        x -= dw;
        tft.getTextBounds(textToShow.c_str(), x, y, &x1, &y1, &w, &h); 
        //  Serial.print("pozycja kursora=");
        //  Serial.println(x);
        //  Serial.print("koniec napisu=");
        //  Serial.println(w);
        //   Serial.print("napis=");
        //  Serial.println(textToShow.c_str());
         
        while(x+w>wid){
            textToShow=textToShow.substring(0,textToShow.length()-1);
            tft.getTextBounds(textToShow.c_str(), x, y, &x1, &y1, &w, &h);
        }
        tft.setCursor(x, y);
        tft.print(textToShow.c_str());
        if (f == NULL) tft.print("   "); //rubout trailing chars
        tft.setTextWrap(true);
        tft.setTextColor(clockText);
        tft.setTextSize(ClockTextSize);
        tft.setCursor(timePosX+TEXTMARGIN,timePosY);
        int16_t x1, y1;
        uint16_t w, h;
        // tft.getTextBounds(time.c_str(), tft.getCursorX(),tft.getCursorY(), &x1, &y1, &w, &h);
        tft.fillRect(timePosX,timePosY,320-timePosX,45,INSIDE);
        tft.setCursor(timePosX+TEXTMARGIN,timePosY);
        tft.println(time);
        tft.setTextWrap(false);
        tft.setFont(f);
        tft.setTextColor(infoText, INSIDE);
        tft.setTextSize(sz);
        delay(10);
       
    // Serial.print("w for");
    }
// Serial.print("koniec for");
}
void showHello()
{
  // display.setRotation(3); //even = portrait, odd = landscape
  // display.fillScreen(GxEPD_WHITE);
  // display.drawBitmap(error, 0, 0, 296, 128, GxEPD_WHITE);
  // const GFXfont* f = &FreeSansBoldOblique24pt7b;
  // display.setTextColor(GxEPD_BLACK);
  // display.setFont(f);
  // display.setCursor(70,100);
  // display.println(name);
  // display.update();
}
void showClock(String time)    
{
tft.setTextColor(clockText);
tft.setTextSize(ClockTextSize);
tft.setCursor(timePosX+TEXTMARGIN,timePosY);
int16_t x1, y1;
uint16_t w, h;
tft.getTextBounds(time.c_str(), tft.getCursorX(),tft.getCursorY(), &x1, &y1, &w, &h);
tft.fillRect(x1,y1,w,h,INSIDE);
tft.setCursor(timePosX+TEXTMARGIN,timePosY);
tft.println(time);
}
void showInfo(String info, String time)    
{
tft.setTextColor(infoText);
tft.setTextSize(infoTextSize);
tft.setCursor(infoPosX+TEXTMARGIN,infoPosY);

scrolltext(tft.getCursorX(), tft.getCursorY(),210, info.c_str(),time,12, NULL, 2);
}

void showTermostatVAalOnly(int posX, int posY,int actualVal ){
 
tft.drawRect(posX,posY,TermoWidth,TermoHeight,BORDER);
tft.drawRect(posX+BORDERWIDTH,posY+BORDERWIDTH,TermoWidth-(BORDERWIDTH/2)-TermoButon-(BORDERWIDTH/2),TermoHeight-(BORDERWIDTH*2),BORDER);
tft.drawRect(posX+BORDERWIDTH+TermoWidth-TermoButon,posY+BORDERWIDTH,TermoButon-(BORDERWIDTH*2),TermoButon-(BORDERWIDTH),BORDER);
tft.drawRect(posX+BORDERWIDTH+TermoWidth-TermoButon,
                        posY+TermoButon+BORDERWIDTH,
                        TermoButon-(BORDERWIDTH*2),
                        TermoButon-(BORDERWIDTH*2)
                        ,BORDER);

// tft.setFont(&FreeSansBoldOblique24pt7b);
tft.setTextColor(TermoValText);
tft.setTextSize(tempValTextSize);
tft.setCursor(posX+BORDERWIDTH+TEXTMARGIN,posY+(TermoHeight/2)-(tempValTextSize*4));

tft.println(actualVal);


}
void showTermostat(int posX, int posY,int actualVal,String place){
 
tft.drawRect(posX,posY,TermoWidth,TermoHeight,BORDER);
tft.drawRect(posX+BORDERWIDTH,posY+BORDERWIDTH,TermoWidth-(BORDERWIDTH/2)-TermoButon-(BORDERWIDTH/2),TermoHeight-(BORDERWIDTH*2),BORDER);
tft.drawRect(posX+BORDERWIDTH+TermoWidth-TermoButon,posY+BORDERWIDTH,TermoButon-(BORDERWIDTH*2),TermoButon-(BORDERWIDTH),BORDER);
tft.drawRect(posX+BORDERWIDTH+TermoWidth-TermoButon,
                        posY+TermoButon+BORDERWIDTH,
                        TermoButon-(BORDERWIDTH*2),
                        TermoButon-(BORDERWIDTH*2)
                        ,BORDER);

// tft.setFont(&FreeSansBoldOblique24pt7b);
tft.setTextColor(TermoInfoText);
tft.setTextSize(tempInfoTextSize);
tft.setCursor(posX+BORDERWIDTH+TEXTMARGIN,posY+BORDERWIDTH+TEXTMARGIN);

tft.println(place);
tft.setCursor(posX+BORDERWIDTH+TEXTMARGIN,tft.getCursorY()+TEXTMARGIN);

tft.setTextSize(tempValTextSize);
tft.setTextColor(TermoValText);
tft.println(actualVal);


}
void showTermometr(int posX, int posY,int actualVal, String place){
  
tft.drawRect(posX,posY,TermoWidth-TermoButon,TermoHeight,BORDER);
tft.drawRect(posX+BORDERWIDTH,posY+BORDERWIDTH,TermoWidth-(BORDERWIDTH/2)-TermoButon-(BORDERWIDTH*1.5),TermoHeight-(BORDERWIDTH*2),BORDER);

// tft.setFont(&FreeSansBoldOblique24pt7b);
tft.setTextColor(TermoInfoText);
tft.setTextSize(tempInfoTextSize);
tft.setCursor(posX+BORDERWIDTH+TEXTMARGIN,posY+BORDERWIDTH+TEXTMARGIN);

tft.println(place);
tft.setCursor(posX+BORDERWIDTH+TEXTMARGIN,tft.getCursorY()+TEXTMARGIN);

tft.setTextSize(tempValTextSize);
tft.setTextColor(TermoValText);
tft.println(actualVal);


}
void showMainMenu(int position){

}
//  void showMenu(Screens e,int position)
// {
//   //   display.setRotation(2); //even = portrait, odd = landscape
//   // display.fillScreen(GxEPD_WHITE);
//   // const GFXfont* f = &FreeMonoBold9pt7b ;
//   // display.setTextColor(GxEPD_BLACK);
//   // display.setFont(f);
//   // display.setCursor(0,10);
//   // // opcja 
//   // for(int i=0;i<e.sizeOfMove;i++){
//   // if (position==i){
//   //   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   //   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   //   }
//   // display.drawBitmap(&(*(e.menuItems+i)->icon), positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // // display.drawBitmap(options, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // // opcja 2
//   // positionX+=64;
//   // if (positionX>=displayWidth){
//   // positionX-=displayWidth;
//   // positionY+=64;}
  
//   // }
//   // display.update();
//   }

void initdisplay(){
   Serial.begin(9600);
  Serial.println(F("TFT LCD test"));

  #ifdef USE_ADAFRUIT_SHIELD_PINOUT
  Serial.println(F("Using Adafruit 2.4\" TFT Arduino Shield Pinout"));
  #else
  Serial.println(F("Using Adafruit 2.4\" TFT Breakout Board Pinout"));
  #endif

  Serial.print("TFT size is "); Serial.print(tft.width()); Serial.print("x"); Serial.println(tft.height());

  tft.reset();

  uint16_t identifier = tft.readID();
  if(identifier==0x0101)
      identifier=0x9341;
  
  if(identifier == 0x9325) {
    Serial.println(F("Found ILI9325 LCD driver"));
  } else if(identifier == 0x4535) {
    Serial.println(F("Found LGDP4535 LCD driver"));
  }else if(identifier == 0x9328) {
    Serial.println(F("Found ILI9328 LCD driver"));
  } else if((identifier == 0x7575)||(identifier == 0x75)||(identifier == 0x875)) {
    identifier=0x7575;
    Serial.println(F("Found HX8347G LCD driver"));
  } else if(identifier == 0x9341) {
    Serial.println(F("Found ILI9341 LCD driver"));
  } else if(identifier == 0x8357) {
    Serial.println(F("Found HX8357D LCD driver"));
  } else {
    Serial.print(F("Unknown LCD driver chip: "));
    Serial.println(identifier, HEX);
    Serial.println(F("If using the Adafruit 2.4\" TFT Arduino shield, the line:"));
    Serial.println(F("  #define USE_ADAFRUIT_SHIELD_PINOUT"));
    Serial.println(F("should appear in the library header (Adafruit_TFT.h)."));
    Serial.println(F("If using the breakout board, it should NOT be #defined!"));
    Serial.println(F("Also if using the breakout, double-check that all wiring"));
    Serial.println(F("matches the tutorial."));
    return;
  }
  
  tft.begin(identifier);
   tft.setRotation(1);
  tft.fillScreen(INSIDE);
  
  //Init SD_Card
  pinMode(10, OUTPUT);
   
  if (!SD.begin(10)) {
    Serial.println("initialization failed!");
    tft.setCursor(0, 0);
    tft.setTextColor(WHITE);    
    tft.setTextSize(1);
    tft.println("SD Card Init fail.");   
  }else
  Serial.println("initialization done."); 
  }

void showAP (){
  // display.setRotation(3); //even = portrait, odd = landscape
  // display.fillScreen(GxEPD_WHITE);
  // const GFXfont* f = &FreeMonoBold9pt7b ;
  // display.setTextColor(GxEPD_BLACK);
  // display.setFont(f);
  // display.setCursor(0,50);
  // display.println("Connect to Badgy AP");
  // display.println("to setup your WiFi!");
  // display.update();  
}

