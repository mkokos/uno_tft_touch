// #include"Screens.h";
#include "Arduino.h"
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library
#include <SD.h>
#include <SPI.h>
void showHello();

void showAP();
void showText(char *text);
void showMainMenu(int position);
// void showMenu(Screens e,int position);
// void showStart(screenName e ,int position);
void initdisplay();
void showIP(char urlCharArray[]);
uint32_t read32(File f);
uint16_t read16(File f);
void bmpdraw(File f, int x, int y);
void showTermostat(int posX, int posY,int actualVal, String place );
void showTermometr(int posX, int posY,int actualVal, String place);
void showClock(String time) ;
void showInfo(String info,String time)    ;
void scrolltext(int x, int y,int boxW, String s, String time, uint8_t dw = 1, const GFXfont *f = NULL, int sz = 1);
